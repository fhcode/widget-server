require("dotenv").config();

const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;

const app = require("../index.js");

chai.should();
chai.use(chaiHttp);

describe("Photo widget server tests", () => {
  
  it("responds with HTTP 200 if successful home page request", (done) => {
    chai.request(app)
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      })
  });

  it("responds with HTTP 200 for successful photo widget request", (done) => {
    chai.request(app)
      .get("/widgets/child-photo/0500050296")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      })
  });

  it("responds with HTTP 404 for non-successful non-existant photo widget request", (done) => {
    chai.request(app)
      .get("/widgets/child-photo/12345678")
      .end((err, res) => {
        expect(res).to.have.status(404);
        done();
      })
  });

})


