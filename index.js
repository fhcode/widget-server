const express = require('express');
const morgan = require('morgan');
const request = require('request');

const app = express();
app.use(morgan('dev'));
app.use(express.static('public'));

//---- TEST STUFF ----//
const hcPhotoUrl = "https://alpha-api.fh.org/dmsapi/child/photo/0500050296_20151207_20151207122006_P16.jpg";
const hcPhotoUrl2 = "https://alpha-api.fh.org/dmsapi/child/photo/0500050296_20151207_XXXXXXXXXXXXXX_P16.jpg";

//---- CONSTANTS ----//

const httpExpireMs = 86400 * 1000;
const port = (process.env.ENVMODE && process.env.ENVMODE == 'DEV')
  ? 3232
  : 80;

//---- HELPERS --//

let httpExpiresGMTString = () => {
  const cts = Date.now();
  let expireDate = new Date(cts + httpExpireMs);
  return expireDate.toGMTString();
}

let photoApiUrl = (cid) => {
  return `https://alpha-api2.fh.org/api/v1/children/search?ChildCode=${cid}`;
};

//---- ROUTES --//

app.get('/', (req, res) => {
  res.send("O Lord, let my soul rise up to meet you");
});

app.get('/help', (req, res) => {
  const hm = "/widgets/child-photo/{{cid}} is the ticket you need.";
  res.send(hm);
});

app.get('/widgets/child-photo/:cid', (req, res, next) => {
  const cid = req.params.cid;
  const photourl = photoApiUrl(cid);
  request(photourl, (err, resp, body) => {
    console.log("Error?: ", err);
    let childrec = {};
    try {
      childrec = JSON.parse(body);
      // CHECK FOR VALID PHOTO VALUE //
      if (childrec.data && childrec.data.length > 0 && childrec.data[0].LargePhoto) {
        let purl = childrec.data[0].LargePhoto;
        res.set('Cache-Control', 'max-age=86400');
        res.set('Expires', httpExpiresGMTString());
        res.set('Pragma', 'public');
        req.pipe(request(purl)).pipe(res);
      } else {
        console.log(`No child record data for child: ${cid}`);
        next()
      }
    } catch (e) {
      console.log(`Error parsing child record: ${e}`);
      next(e);
      // SEND DEFAULT PHOTO? //
    }
  });
});

app.use((req, res) => {
  res.statusCode = 404;
  res.end("What have you wrought?");
});

//---- RUN SERVER RUN ----//

app.listen(port, () => console.log(`App listening on port ${port}!`));

module.exports = app;
