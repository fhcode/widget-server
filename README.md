# give.fh.org Widget Server

## Node JS / Express / Mocha + Chai

The repository contains widget utilities, setup as a Node JS Express server, to substitute functions provided by depracated Expression Engine (give.fh.org) `mod.fh.php`. 

First route is a photo server widget -- to pass through a photo, using same HTTP request format:

`/widgets/child-photo/{CHILD_CODE}`

Child code is a numeric id that is passed through to `(alpha-)?api2.fh.org/api/v1/children/search?ChildCode=?`. A JSON structure is returned that includes a URL for a photo image in `data[0].LargePhoto`. App will then retrieve photo and pass through to the requester.

## Installation

```npm install```

```npm start```

App can be run on `localhost` or as a Docker container. Docker container exposes port 80 and is setup to run behind a nginx-proxy server. See separate documentation for instructions on setting and configuring that. 

There is an environment setting (contained in `.env`) that will set `ENVMODE` to `DEV` and then server port will be set to 3232. Otherwise it will default to 80.


## Testing

Uses mocha, chai, and chai-http to perform some basic testing.

```npm test```

### Contact

* George Trifanoff <gtrifanoff@fh.org>